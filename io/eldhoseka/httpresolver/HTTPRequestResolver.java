package io.eldhoseka.httpresolver;

import java.io.*;
import java.net.*;

public class HTTPRequestResolver{
	String reqURL;
	int reqType;
	long start,end;
	String reqMethod;
	String routePart1;
	String routePart2;
	
	public HTTPRequestResolver(String request){
		int i;
		//System.out.print("\n***\n"+request+"**"+request.length()+"**\n");
		String il[] = request.trim().split("\n");
		//System.out.print("\nil[0]"+il[0]);
		reqURL = il[0].trim().split(" ")[1];
		if(reqURL.length()>1)
			routePart1 = reqURL.replace("/"," ").trim().split(" ")[0];
		else
			routePart1 = "";
		routePart2 = "";
		String path[] = reqURL.replace("/"," ").trim().split(" ");
		try{
		for(i=1;i<path.length;++i)
			routePart2=routePart2+File.separator+URLDecoder.decode(path[i],"UTF-8");
		}catch(Exception e){
			routePart2="";
		}
		reqMethod = il[0].trim().split(" ")[0];
		reqType=200;
		start=end=(long)(-1);
		for(i=1;i<il.length;++i){
			String headerlist[] = il[i].split(": ");
			if(headerlist.length>=2){
				if(headerlist[0].compareTo("Range")==0){
					reqType=206;
					headerlist[1]=headerlist[1].replace("bytes=","").trim();
					if(headerlist[1].length()>=2){
						if(headerlist[1].charAt(0)=='-'){
							start=(long)0;
						}
						else{
							start=Long.parseLong(headerlist[1].split("-")[0]);
						}
						if(headerlist[1].charAt(headerlist[1].length()-1)!='-'){
							end=Long.parseLong(headerlist[1].split("-")[1]);
						}
					}
					else{
						start=(long)0;
					}
				}
			}
		}
	}
	
	public boolean isHTTP206Request(){
		if(reqType==206)
			return true;
		else
			return false;
	}
	
	public boolean isRangeError(long maxsize){
		if(reqType!=206)
			return false;
		if(start>end && end!=-1)
			return true;
		else if(start>=maxsize||end>=maxsize||start<0)
			return true;
		return false;
	}
	
	public long contentLength(long maxsize){
		if(reqType==200)
			return maxsize;
		if(end==-1)
			return (maxsize-start);
		else
			return (end-start+1);
	}
	
	public long rangeStart(){
		if(reqType==200)
			return 0;
		return start;
	}
	
	public long rangeEnd(long maxsize){
		if(reqType==200||end==-1)
			return (maxsize-1);
		return end;
	}
	
	public String requestURL(){
		return reqURL;
	}
	
	public String requestMethod(){
		return reqMethod;
	}
	
	public String serviceType(){
		return routePart1;
	}
	
	public String requestFile(){
		return routePart2;
	}
	
}