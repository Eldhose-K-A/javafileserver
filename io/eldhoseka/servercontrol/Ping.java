package io.eldhoseka.servercontrol;

import java.net.*;
import java.io.*;

public class Ping{
	
	public static void selfPing(int port){
		try{
			Socket s1 = new Socket("localhost",port);
			OutputStream o1 = s1.getOutputStream();
			String msg="GET / HTTP/1.1\n\n";
			o1.write(msg.getBytes());
			o1.close();
			s1.close();
			//System.out.print("\nPinged!........1\n");
		}
		catch(Exception e){
			//System.out.print("\nPinged!........2\n");
			//e.printStackTrace();
			;
		}
	}
	
	public static void ping(String address,int port){
		try{
			Socket s1 = new Socket(address,port);
			OutputStream o1 = s1.getOutputStream();
			String msg="GET / HTTP/1.1\n\n";
			o1.write(msg.getBytes());
			o1.close();
			s1.close();
			//System.out.print("\nPinged!........1\n");
		}
		catch(Exception e){
			//System.out.print("\nPinged!........2\n");
			//e.printStackTrace();
			;
		}
	}
	
}