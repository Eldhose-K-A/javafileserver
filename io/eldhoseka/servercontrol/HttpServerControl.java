package io.eldhoseka.servercontrol;

import java.io.*;
import java.net.*;

import io.eldhoseka.router.ClientHandler;

public class HttpServerControl implements Runnable{
	String rootPath;
	public Thread Tid;
	boolean exitCondition;
	
	public HttpServerControl(String rootPath){
		this.rootPath = rootPath;
		this.exitCondition = false;
		this.Tid = new Thread(this,"ServerThread");
		Tid.start();
	}
	
	public void stopServer(){
		this.exitCondition = true;
	}
	
	public void run(){
		System.out.println("\nServer Thread Started!............. # ");
		while(!this.exitCondition){
			try{
				ServerSocket ss1 = new ServerSocket(3000);
				Socket s1;
				while(!this.exitCondition){
					s1=ss1.accept();
					new ClientHandler(s1,rootPath);
					Thread.sleep(100);
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}