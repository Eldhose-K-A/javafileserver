package io.eldhoseka.router;

import java.io.*;
import java.net.*;

import io.eldhoseka.httpresolver.HTTPRequestResolver;

public class ClientHandler implements Runnable{
	Thread t1;
	Socket s1;
	String rootPath;
	
	public ClientHandler(Socket s1,String rootPath){
		this.s1=s1;
		this.rootPath = rootPath;
		this.t1 = new Thread(this,"Client");
		this.t1.start();
	}
	
	public void run(){
		try{
			String res;
			int ERROR_FLAG=0;
			long MAX_BUFFER = 102300;
			InputStream i1 = this.s1.getInputStream();
			OutputStream o1 = this.s1.getOutputStream();
			File iFile=null;
			FileInputStream fin=null;
	
			while(i1.available()<=0);
			
			byte inputb[] = new byte[i1.available()];
			i1.read(inputb);
			String inputs = new String(inputb);
			
			HTTPRequestResolver h1 = new HTTPRequestResolver(inputs);
			System.out.print("\n"+h1.requestMethod()+" :-> "+h1.requestURL()+"\n\tFile\t: "+h1.serviceType()+h1.requestFile()+"\n\tAPI\t: "+h1.serviceType()+"\n# ");

			if((h1.serviceType().compareTo("view")==0)||(h1.serviceType().compareTo("download")==0)){
				try{
					iFile = new File(rootPath+h1.requestFile());
					if(iFile.exists() && iFile.isFile())
						fin = new FileInputStream(iFile);
				}
				catch(Exception e)
				{
					ERROR_FLAG=2;
					e.printStackTrace();
				}
			}
			
			byte buffer[] = new byte[(int)MAX_BUFFER];
			
			if(h1.serviceType().compareTo("")==0){
				iFile = new File(System.getProperty("user.dir")+File.separator+"public"+File.separator+"index.html");
				
				res="HTTP/1.1 200 OK\nServer: FileServer\nAccept-Ranges: bytes\nContent-Type: text/html\n\n";
				try{
					if(iFile.exists() && iFile.isFile()){
						fin = new FileInputStream(iFile);
						byte htmlfileb[] = new byte[fin.available()];
						fin.read(htmlfileb);
						o1.write(res.getBytes());
						o1.flush();
						o1.write(htmlfileb);
						o1.flush();
					}
					else{
						res=res+"<a href=\"/view\">Redirect</a>";
						o1.write(res.getBytes());
						o1.flush();
					}
				}
				catch(Exception e){
					;
				}
			}
			else if((h1.serviceType().compareTo("javascripts")==0)||(h1.serviceType().compareTo("stylesheets")==0)||(h1.serviceType().compareTo("images")==0)){
				iFile = new File(rootPath+File.separator+"public"+File.separator+h1.serviceType()+h1.requestFile());
				try{
					if(iFile.exists() && iFile.isFile()){
						if(h1.serviceType().compareTo("javascripts")==0)
							res="HTTP/1.1 200 OK\nServer: FileServer\nContent-Type: text/javascript\n\n";
						else if(h1.serviceType().compareTo("stylesheets")==0)
							res="HTTP/1.1 200 OK\nServer: FileServer\nContent-Type: text/css\n\n";
						else if(h1.requestFile().endsWith(".bmp"))
							res="HTTP/1.1 200 OK\nServer: FileServer\nContent-Type: image/bmp\n\n";
						else if(h1.requestFile().endsWith(".png"))
							res="HTTP/1.1 200 OK\nServer: FileServer\nContent-Type: image/png\n\n";
						else
							res="HTTP/1.1 200 OK\nServer: FileServer\nContent-Type: image/jpeg\n\n";
						
						fin = new FileInputStream(iFile);
						byte publicfileb[] = new byte[fin.available()];
						fin.read(publicfileb);
						o1.write(res.getBytes());
						o1.flush();
						o1.write(publicfileb);
						o1.flush();
					}
					else{
						res="HTTP/1.1 404 Not Found\nServer: FileServer\nContent-Type: text/plain\n\nFile Not Found!.......";
						o1.write(res.getBytes());
						o1.flush();
					}
				}
				catch(Exception e){
					;
				}
			}
			else if(h1.serviceType().compareTo("view")==0){
				if(ERROR_FLAG==2){
					res="HTTP/1.1 404 Not Found\nContent-Type: application/json\n\n"+"{ \"path\" : \""+h1.requestFile().replace("\\","/")+"\" , \"contents\" : [] , \"type\" : [] , \"errorstatus\" : \"1\" , \"errormsg\" : \"No Such Directory\" }";
				}
				else if(iFile.isDirectory()){
					String filelist="";
					String typelist="";
					String dirls[] = iFile.list();
					if(dirls.length>=1){
						filelist = filelist + " \"" + dirls[0] + "\"";
						if((new File(iFile,dirls[0])).isDirectory())
							typelist = typelist + " \"1\"";
						else
							typelist = typelist + " \"0\"";
					}
					int y = 1;
					while(y<dirls.length){
						filelist = filelist + " , \"" + dirls[y] + "\"";
						if((new File(iFile,dirls[y])).isDirectory())
							typelist = typelist + " , \"1\"";
						else
							typelist = typelist + " , \"0\"";
						y=y+1;
					}
					res="HTTP/1.1 200 OK\nContent-Type: application/json\n\n"+"{ \"path\" : \""+h1.requestFile().replace("\\","/")+"\" , \"contents\" : ["+filelist+" ] , \"type\" : ["+typelist+" ] , \"errorstatus\" : \"0\" , \"errormsg\" : \"No Error\" }";
				}
				else{
					res="HTTP/1.1 404 Not Found\nContent-Type: application/json\n\n"+"{ \"path\" : \""+h1.requestFile().replace("\\","/")+"\" , \"contents\" : [] , \"type\" : [] , \"errorstatus\" : \"1\" , \"errormsg\" : \"No Such Directory\" }";
				}
				
				o1.write(res.getBytes());
				o1.flush();
			}
			else if(h1.serviceType().compareTo("download")==0){
				if(ERROR_FLAG==2||!iFile.exists()){
					res="HTTP/1.1 404 Not Found\nContent-Type: text/html\n\n<h1>File Not Found!.....</h1>";
					o1.write(res.getBytes());
					o1.flush();
				}
				else if(iFile.isDirectory()){
					res="HTTP/1.1 404 Not Found\nContent-Type: text/html\n\n<h1>File Not Found!.....</h1>";
					o1.write(res.getBytes());
					o1.flush();
				}
				else{
					if(!h1.isHTTP206Request()){
						res="HTTP/1.1 200 OK\nServer: FileServer\nAccept-Ranges: bytes\nContent-Length: "+h1.contentLength(fin.available());
						res=res+"\nContent-Type: application/octet-stream\nContent-Disposition: attachment; filename=\""+iFile.getName()+"\"\n\n";
				
						o1.write(res.getBytes());
						o1.flush();
						while(fin.available()>0){
							if(fin.available()<MAX_BUFFER)
								buffer = new byte[fin.available()];
							fin.read(buffer);
							o1.write(buffer);
							o1.flush();
						}
					}
					else{ 
						if(!h1.isRangeError(fin.available())){
							res="HTTP/1.1 206 Partial Content\nServer: FileServer\nAccept-Ranges: bytes\nContent-Length: "+h1.contentLength(fin.available());
							res=res+"\nContent-Range: bytes "+h1.rangeStart()+"-"+h1.rangeEnd(fin.available())+"/"+fin.available()+"\nContent-Type: application/octet-stream";
							res=res+"\nContent-Disposition: attachment; filename=\""+iFile.getName()+"\"\n\n";
					
							o1.write(res.getBytes());
							o1.flush();
							fin.skip(h1.rangeStart());
							while(fin.available()>0){
								if(fin.available()<MAX_BUFFER)
								buffer = new byte[fin.available()];
								fin.read(buffer);
								o1.write(buffer);
								o1.flush();
							}
						}
						else{
							System.out.print("\nRange Error\nStart : "+h1.rangeStart()+" End : "+h1.rangeEnd(fin.available())+" Max : "+fin.available()+" # ");
						}
					}
				}
			}
			else{
				res="HTTP/1.1 404 Not Found\n\nError Resorce Not Found!....";
				o1.write(res.getBytes());
				o1.flush();
			}
			if(iFile!=null){
				if(iFile.exists() && iFile.isFile())
					fin.close();
			}
			//---------------------------------------------------------------------------------------------------------------------------------
			i1.close();
			o1.close();
			this.s1.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
