import io.eldhoseka.servercontrol.*;
import io.eldhoseka.router.*;
import io.eldhoseka.servercontrol.*;

import java.util.*;

class Demo{
		
	public static void main(String args[]){
		try{
			Scanner sin = new Scanner(System.in);
			String instr;
			HttpServerControl ser = new HttpServerControl("/home/mits/Desktop");
			while(true){
				instr = sin.nextLine();
				if(instr.compareTo("quit")==0){
					try{
						if(ser.Tid.isAlive()){
							ser.stopServer();
							Ping.selfPing(3000);
							ser.Tid.join();
							return;
						}
					}catch(Exception e){
						System.out.println("\n\nError Stopping Server Thread!.......(1)\n");
					}
					return;
				}
				else if(instr.compareTo("stop")==0){
					try{
						ser.stopServer();
						Ping.selfPing(3000);
						ser.Tid.join();
						System.out.println("\n\nServer Thread Stopped!.......(2)");
					}catch(Exception e){
						System.out.println("\n\nError Stopping Server Thread!.......(2)");
					}
				}
				else{
					System.out.println("\n\n------------->INVALID INSTRUCTION<--------------\n");
				}
			}
		}
		catch(Exception e){
			System.out.println("\nMajor Error\n");
		}
	}
}
